from django.http import JsonResponse
from django.shortcuts import render
from django.views.generic import View
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import authentication, permissions
from django.contrib.auth.models import User
from college.models import *
from mazintech.models import *

class courseTotalEnrolmentData(APIView):
    authentication_classes = []
    permission_classes = []

    def get(self, request, format=None):
        courses = Course.objects.exclude(deleted=True).order_by('course_name')
        totals = []
        labels = []
        for course in courses:
            total = course.total_enrolment
            label = course.course_name
            totals.append(total)
            labels.append(label)

        data = {
            'labels':labels, 
            'values':totals,
            }

        return Response(data)


class currentTotalEnrolmentData(APIView):
    authentication_classes = []
    permission_classes = []

    def get(self, request, format=None):
        courses = Course.objects.exclude(deleted=True).order_by('course_name')
        totals = []
        labels = []
        for course in courses:
            total = course.current_enrolment
            label = course.course_name
            totals.append(total)
            labels.append(label)

        data = {
            'labels':labels, 
            'values':totals,
            }

        return Response(data)

class regionalTotalEnrolmentData(APIView):
    authentication_classes = []
    permission_classes = []

    def get(self, request, format=None):
        regions = Region.objects.all()
        totals = []
        labels = []
        for region in regions:
            total = region.student_region.all().count()
            label = region.name
            totals.append(total)
            labels.append(label)

        data = {
            'labels':labels,
            'values':totals,
            }
        return Response(data)

