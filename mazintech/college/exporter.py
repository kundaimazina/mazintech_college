import os
import csv
import json
import xmltodict
from lib.excel_helper import ExcelHelper
from lib.file_handler import file_download, save_file
from datetime import datetime, timedelta
from django.utils import timezone
from django.conf import settings
from django.core.files import File
from django.http import HttpResponse
from mazintech.celery import app
from celery.contrib import rdb
from celery.schedules import crontab
from models import *
from datetime import datetime
from django.db import transaction
import sys
from django.db.models import Sum, Count
from django.db.models import Q, F, Case, When, FloatField, IntegerField, Value


reload(sys)
sys.setdefaultencoding('utf8')

import logging
logger = logging.getLogger(__name__)

@app.task(name='test')
def test():
    print 'celery beat is working'


@app.task(name='exporter.students_data')
def export_student_data(user, sheet_name, students, data):
    row_number = 1
    try:
        with transaction.atomic():

            file_name = "Student Details Extract - %s.csv" %(datetime.now().strftime("%Y%m%d_%H%M%S"))
            csv_file = os.path.join(settings.MEDIA_ROOT, 'uploads/documents/', file_name)
            f = open(csv_file, "w+b")
            f.truncate()
            writer = csv.writer(f)

            writer.writerow(['Students as of:', '{}'.format(datetime.strftime(datetime.now(), '%b %d, %Y'))])

            writer.writerow([''])

            headers = ['First Name','Surname','DOB','ID/Passport','Course','Course Year']

            
            writer.writerow(headers)

            rows = [writer.writerow([s.first_name,
                                     s.surname,
                                     s.date_of_birth,
                                     s.id_number if s.id_number else s.passport_number,
                                     s.course,
                                     s.course_year,
                                     ])
                    for s in students]
            
            return save_file(user, file_name, csv_file, "Download","Students Extract")
        
    except Exception, ex:
        logger.exception(ex)
        raise Exception("Failed while processing export. Error was %s" % (ex))


@app.task(name='exporter.enrolment_data')
def export_enrolment_data(user, sheet_name, enrolments, data):
    row_number = 1
    report_name = 'Student Enrolment Extract'
    
    try:
        with transaction.atomic():
            file_name = '{} Extract - {}.csv'.format(report_name, datetime.strftime(datetime.now(), '%Y%m%d_%H%M%S'))
            
            csv_file = os.path.join(settings.MEDIA_ROOT, 'uploads/documents/', file_name)
            f = open(csv_file, "w+b")
            f.truncate()
            writer = csv.writer(f)
            
            writer.writerow(['{}s as of:'.format(report_name), '{}'.format(datetime.strftime(datetime.now(), '%b %d, %Y'))])

            writer.writerow([''])

            headers = ['First Name','Surname','Course','Course Year', 'Signed On', 'Processed', 'Application Fee',]

            writer.writerow(headers)
            rows = [writer.writerow([e.student.first_name if e.student else '',
                                     e.student.surname if e.student else '',
                                     e.course.course_name if e.course else '',
                                     e.enrolment_year,
                                     datetime.strftime(e.application_signed, '%Y-%m-%d') \
                                                       if e.application_signed else '',
                                     datetime.strftime(e.created_at, '%Y-%m-%d') \
                                                       if e.created_at else '',
                                     e.application_fee])
                    for e in enrolments]

            
            return save_file(user, file_name, csv_file, "Download",report_name)
        
    except Exception, ex:
        logger.exception(ex)
        raise Exception("Failed while processing export. Error was %s" % (ex))

@app.task(name='exporter.course_data')
def extract_course_data(user, sheet_name, courses, data):

    row_number = 1

    try:
        with transaction.atomic():
            file_name = 'Courses Extract - {}.csv'.format(
                datetime.strftime(datetime.now(), '%Y%m%d_%H%M%S'))
            csv_file = os.path.join(settings.MEDIA_ROOT, 'uploads/documents/', file_name)
            f = open(csv_file, "w+b")
            f.truncate()
            writer = csv.writer(f)

            writer.writerow(['Courses as of:', '{}'.format(datetime.strftime(datetime.now(), '%b %d, %Y'))])

            writer.writerow([''])

            headers = ['Course Name','Current Enrolment','Total Enrolment']

            
            writer.writerow(headers)

            rows = [writer.writerow([c.course_name,
                                     c.current_enrolment,
                                     c.total_enrolment,
                                    ])
                    for c in courses]

            
            return save_file(user, file_name, csv_file, "Download","Courses Extract")
        
    except Exception, ex:
        logger.exception(ex)
        raise Exception("Failed while processing export. Error was %s" % (ex))


@app.task(name='exporter.user_data')
def extract_user_data(user, sheet_name, users, data):

    row_number = 1

    try:
        with transaction.atomic():
            file_name = 'User Extract - {}.csv'.format(
                datetime.strftime(datetime.now(), '%Y%m%d_%H%M%S'))
            csv_file = os.path.join(settings.MEDIA_ROOT, 'uploads/documents/', file_name)
            f = open(csv_file, "w+b")
            f.truncate()
            writer = csv.writer(f)

            writer.writerow(['Users as of:', '{}'.format(datetime.strftime(datetime.now(), '%b %d, %Y'))])

            writer.writerow([''])

            headers = ['User Name','First Name','Surname','Phone Number','Email']
            
            writer.writerow(headers)

            rows = [writer.writerow([emp.username,
                                     emp.first_name,
                                     emp.last_name,
                                     emp.phone_number,
                                     emp.email,
                                    ])
                    for emp in users]

            
            return save_file(user, file_name, csv_file, "Download","Users Extract")
        
    except Exception, ex:
        logger.exception(ex)
        raise Exception("Failed while processing export. Error was %s" % (ex))