from django.conf.urls import url, include
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.auth import views as auth_views
from django.contrib import admin
from dal import autocomplete

import views
from college.graphs import *
# from mazintech.autocomplete import *

from .models import *
from mazintech.models import *

urlpatterns = [
    url(r'^student/edit/(?P<student_id>\d+)/$', views.edit_student, name='edit_student'),
    url(r'^student/add/$', views.edit_student, name='add_student'), 
    url(r'^students/$', views.students, name='students'),
    url(r'^enrolments/$', views.enrolments, name='enrolments'),
    url(r'^courses/$', views.courses, name='courses'),
    url(r'^student-enrolment/edit/(?P<enrolment_id>\d+)/$', views.edit_enrolment, name='edit_enrolment'),
    url(r'^student-enrolment/add/(?P<student_id>\d+)/$', views.edit_enrolment, name='add_enrolment'),
    url(r'^student-enrolment/$', views.edit_enrolment, name='add_new_enrolment'),
    url(r'^course/edit/(?P<course_id>\d+)/$', views.edit_course, name='edit_course'),
    url(r'^course/add/$', views.edit_course, name='add_course'),
    url(r'^course-delete/$', views.delete_course, name='delete_course'),
    url(r'^student-delete/$', views.delete_student, name='delete_student'),
    url(r'^enrolment-delete/$', views.delete_enrolment, name='delete_enrolment'),
    url(r'^course-totals/$', courseTotalEnrolmentData.as_view(), name='course_totals'),
    url(r'^current-totals/$', currentTotalEnrolmentData.as_view(), name='current_totals'),
    url(r'^regional-totals/$', regionalTotalEnrolmentData.as_view(), name='regional_totals'),
    
]

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)


