import os
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render
from django.shortcuts import render, redirect, get_object_or_404, render_to_response
from django.template import RequestContext
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib import messages
from django.http import HttpResponse, HttpResponseRedirect
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings
from django.db.models import Count, Sum, Q, F, ExpressionWrapper, IntegerField
from django.db.models.functions import Concat
from django.db.models import Value as V
import json
import threading

from django.http import JsonResponse
from django.shortcuts import render
from django.views.generic import View
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import authentication, permissions
from django.contrib.auth.models import User

from models import *
from forms import *
from exporter import *
from mazintech.models import *
from mazintech.forms import AddressForm, ContactForm
from dal import autocomplete
from datetime import datetime

@login_required
@csrf_exempt
def get_districts(request, region_id):
    if region_id:
        districts = Branch.objects.filter(region__id=region_id)
    else:
        districts = Branch.objects.all()

    data = []

    for d in districts:
        district = {}
        district['id'] = d.id
        district['label'] = d.code +' - '+ d.description
        district['value'] = d.code +' - '+ d.description
        data.append(district)

    results = json.dumps(data)
    mimetype = 'application/json'
    return HttpResponse(results, mimetype)

@login_required
def students(request, template="college/students.html"):
    context = {}
    filter_form = StudentFilterForm(request.GET or None)
    district_id = request.user.get_user_district
    region_id = request.user.get_user_region
    is_regional_user = request.user.is_regional_user
    
    if request.user.is_superuser or request.user.is_management:
        students = Student.objects.all()
    elif district_id and is_regional_user:
        students = Student.objects.filter(district__id=district_id)
    elif region_id and is_regional_user: 
        students = Student.objects.filter(region__id=region_id)
    else:
        students = Student.objects.none()

    if u'search' in request.GET or u'extract' in request.GET:
        if filter_form.is_valid():
            students = filter_form.filter(students)

    if u'extract' in request.GET:
        if settings.USE_CELERY:
            export_student_data.delay(request.user, 'all_students',
                                      students, filter_form.cleaned_data)
        else:
            download_thread = threading.Thread(target=export_student_data,
                                               args=(request.user, 'all_students',
                                                     students, filter_form.cleaned_data))
            download_thread.start()
        messages.success(request, 'Your extract has been added to the download queue. Queue processing may take a while. Check your report in downloads.')

    context['all_students'] = students
    context['student_filter_form'] = filter_form

    return render(request, template, context)

@login_required
def edit_student(request, student_id=None, template="college/edit_student.html", context=None):
    context = context or {}
    student =  None
    student_documents = None
    documents = None

    if student_id:
        student = Student.objects.get(pk=student_id)
        student_form = StudentForm(request.POST or None, instance=student)
        student_documents = StudentDocument.objects.filter(student=student)
        documents = Document.objects.all()
    else:
        student_form = StudentForm(request.POST or None)

    student_document_form = StudentDocumentForm(request.POST or None, prefix="documents")
    student_file_form = StudentFileForm(request.POST or None, request.FILES or None, prefix="documents")

    if student_form.is_valid():
        has_errors = False

        if student_file_form.is_valid():
            document = student_file_form.save(commit=False)
        if student_form.has_changed():
            if document.document.name is not None:
                has_errors = False
            else:
                has_errors = True

        if has_errors:
            messages.error(request, "Cannot save Student change, Document upload required")
        else:
            student = student_form.save(commit=False)
            student.created_by = request.user
            student.save()

            if student_file_form.is_valid():
                document = student_file_form.save(commit=False)
                document.document_name = document.document.name
                document.file_type = 'document'
                document.description = 'Student Document'
                if document.document_name is not None and student_document_form.is_valid():
                    document.save()

                    student_document = student_document_form.save(commit=False)
                    student_document.student = student
                    student_document.document = document
                    student_document.created_by = request.user
                    student_document.save()
                    student_document_form.save_m2m()

        if not has_errors:
            return redirect(reverse('college:students'))

    context['student_form'] = student_form
    context['student'] = student
    context['student_id'] = student_id
    context['student_document_form'] = student_document_form
    context['student_file_form'] = student_file_form
    context['student_documents'] = student_documents

    return render(request, template, context)

@login_required
def edit_enrolment(request,enrolment_id=None, student_id=None, template="college/edit_enrolment.html", context=None):
    context = context or {}
    student =  None
    enrolment = None
    student_documents = None
    documents = None

    if student_id:
        student = Student.objects.get(pk=student_id)

    if enrolment_id:
        enrolment = Enrolment.objects.get(pk=enrolment_id)
        student_documents = StudentDocument.objects.filter(student=enrolment.student)
        documents = Document.objects.all()
    else:
        enrolment = Enrolment(student=student)

    enrolment_form = EnrolmentForm(request.POST or None, instance=enrolment)

    student_document_form = StudentDocumentForm(request.POST or None, prefix="documents")
    student_file_form = StudentFileForm(request.POST or None, request.FILES or None, prefix="documents")

    if enrolment_form.is_valid():
        has_errors = False

        if student_file_form.is_valid():
            document = student_file_form.save(commit=False)
        if enrolment_form.has_changed():
            if document.document.name is not None:
                has_errors = False
            else:
                has_errors = True

        if has_errors:
            messages.error(request, "Cannot save Student Enrolment change, Document upload required")
        else:
            enrolment = enrolment_form.save(commit=False)
            enrolment.created_by = request.user
            enrolment.save()

            if student_file_form.is_valid():
                document = student_file_form.save(commit=False)
                document.document_name = document.document.name
                document.file_type = 'document'
                document.description = 'Student Enrolment Document'
                if document.document_name is not None and student_document_form.is_valid():
                    document.save()

                    student_document = student_document_form.save(commit=False)
                    student_document.student = student
                    student_document.document = document
                    student_document.created_by = request.user
                    student_document.save()
                    student_document_form.save_m2m()

        if not has_errors:
            return redirect(reverse('college:students'))

    context['enrolment_form'] = enrolment_form
    context['enrolment'] = enrolment
    context['student_id'] = student_id
    context['enrolment_id'] = enrolment_id
    context['student_document_form'] = student_document_form
    context['student_file_form'] = student_file_form
    context['student_documents'] = student_documents

    return render(request, template, context)


@login_required
def enrolments(request, template="college/enrolments.html"):
    context = {}
    filter_form = EnrolmentFilterForm(request.GET or None)
    students = Enrolment.objects.exclude(deleted=True).order_by('-enrolment_year','-created_at')

    if u'search' in request.GET or u'extract' in request.GET:
        if filter_form.is_valid():
            students = filter_form.filter(students)

    if u'extract' in request.GET:
        if settings.USE_CELERY:
            export_enrolment_data.delay(request.user, 'all_students',
                                        students, filter_form.cleaned_data)

        else:
            download_thread = threading.Thread(target=export_enrolment_data,
                                               args=(request.user, 'all_students',
                                                     students, filter_form.cleaned_data))
            download_thread.start()
        messages.success(request, 'Your extract has been added to the download queue. Queue processing may take a while. Check your report in downloads.')

    context['all_students'] = students
    context['student_filter_form'] = filter_form

    return render(request, template, context)


@login_required
def courses(request, template="college/courses.html"):
    context = {}
    filter_form = CourseFilterForm(request.GET or None)
    courses = Course.objects.exclude(deleted=True).order_by('course_name')

    if u'search' in request.GET or u'extract' in request.GET:
        if filter_form.is_valid():
            courses = filter_form.filter(courses)

    if u'extract' in request.GET:
        if settings.USE_CELERY:
            extract_course_data.delay(request.user, 'all_courses',
                                      courses, filter_form.cleaned_data)
        else:
            download_thread = threading.Thread(target=extract_course_data,
                                               args=(request.user, 'all_courses',
                                                     courses, filter_form.cleaned_data))
            download_thread.start()
        messages.success(request, 'Your extract has been added to the download queue. Queue processing may take a while. Check your report in downloads.')
        if len(set(filter_form.cleaned_data.values())) > 1:
            context['reset_button'] = True

    context['all_courses'] = courses
    context['course_filter_form'] = filter_form

    return render(request, template, context)


@login_required
def edit_course(request, course_id=None, template="college/edit_course.html", context=None):
    context = context or {}
    course =  None

    if course_id:
        course = Course.objects.get(pk=course_id)
    else:
        course = Course()

    course_form = CourseForm(request.POST or None, instance=course)

    if course_form.is_valid():
        course = course_form.save(commit=False)
        course.created_by = request.user
        course.save()
        return redirect(reverse('college:courses'))

    context['course_form'] = course_form
    context['course'] = course
    context['course_id'] = course_id

    return render(request, template, context)

@csrf_exempt
@login_required
def delete_course(request):
    id = None
    course = None
    if request.POST:
        id = request.POST['id']
    if id:
        course = Course.objects.get(id=id)
    if course:
        course.delete(request.user)

    return HttpResponse(json.dumps({'success': True}), 'application/json')

@csrf_exempt
@login_required
def delete_student(request):
    id = None
    student = None
    if request.POST:
        id = request.POST['id']
    if id:
        student = Student.objects.get(id=id)
    if student:
        student.delete(request.user)
    return HttpResponse(json.dumps({'success': True}), 'application/json')

@csrf_exempt
@login_required
def delete_enrolment(request):
    id = None
    enrolment = None
    if request.POST:
        id = request.POST['id']
    if id:
        enrolment = Enrolment.objects.get(id=id)
    if enrolment:
        enrolment.delete(request.user)
    return HttpResponse(json.dumps({'success': True}), 'application/json')

def current_course_totals(request, * args, **kwargs):
    # data={
    #     "sales": 100,
    #     "customers": 10,
    # }
    courses = Course.objects.exclude(deleted=True).order_by('course_name')
    totals = []
    labels = []
    for course in courses:
        total = course.total_enrolment
        label = course.course_name
        totals.append(total)
        labels.append(label)

    data = {
        'labels':labels, 
        'values':totals,
        }

    return JsonResponse(data)


# class courseTotalEnrolmentData(APIView):
#     authentication_classes = []
#     permission_classes = []

#     def get(self, request, format=None):
#         courses = Course.objects.exclude(deleted=True).order_by('course_name')
#         totals = []
#         labels = []
#         for course in courses:
#             total = course.total_enrolment
#             label = course.course_name
#             totals.append(total)
#             labels.append(label)

#         data = {
#             'labels':labels, 
#             'values':totals,
#             }

#         return Response(data)

# class currentTotalEnrolmentData(APIview):
#     authentication_classes = []
#     permission_classes = []

#     def get(self, request, format=None):
#         courses = Course.objects.exclude(deleted=True).order_by('course_name')
#         totals = []
#         labels = []
#         for course in courses:
#             total = course.current_enrolment
#             label = course.course_name
#             totals.append(total)
#             labels.append(label)

#         data = {
#             'labels':labels, 
#             'values':totals,
#             }

#         return Response(data)

# class regionalTotalEnrolmentData(APIview):
#     authentication_classes = []
#     permission_classes = []

#     def get(self, request, format=None):
#         regions = Region.objects.all()
#         totals = []
#         labels = []
#         for region in regions:
#             total = region.student_region.all().count()
#             label = region.name
#             totals.append(total)
#             labels.append(label)

#         data = {
#             'labels':labels,
#             'values':totals,
#             }
#         return Response(data)