# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2018-11-28 05:55
from __future__ import unicode_literals

from django.db import migrations
from college.models import Course
def populate_courses(apps, schema_editor):
    for c in range(1,11):
        course = Course.objects.get_or_create(course_name='Course Programme {}'.format(c))

class Migration(migrations.Migration):

    dependencies = [
        ('college', '0004_auto_20181128_0249'),
    ]

    operations = [
        migrations.RunPython(populate_courses)
    ]
