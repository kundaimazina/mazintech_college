# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2018-11-27 18:37
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import lib.fields


class Migration(migrations.Migration):

    dependencies = [
        ('mazintech', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('college', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='StudentDocument',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True, null=True)),
                ('changed_at', models.DateTimeField(auto_now=True, null=True)),
                ('deleted', models.BooleanField(db_index=True, default=False)),
                ('created_by', lib.fields.ProtectedForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='created_student_documents', to=settings.AUTH_USER_MODEL)),
                ('document', lib.fields.ProtectedForeignKey(on_delete=django.db.models.deletion.PROTECT, to='mazintech.Document')),
            ],
            options={
                'default_permissions': [],
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='StudentDocumentUploads',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True, null=True)),
                ('changed_at', models.DateTimeField(auto_now=True, null=True)),
                ('deleted', models.BooleanField(db_index=True, default=False)),
                ('description', models.CharField(max_length=255)),
            ],
            options={
                'default_permissions': [],
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='historicalstudent',
            name='district',
            field=lib.fields.ProtectedForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='+', to='mazintech.Branch'),
        ),
        migrations.AddField(
            model_name='historicalstudent',
            name='region',
            field=lib.fields.ProtectedForeignKey(blank=True, db_constraint=False, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='+', to='mazintech.Region'),
        ),
        migrations.AddField(
            model_name='student',
            name='district',
            field=lib.fields.ProtectedForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='student_district', to='mazintech.Branch'),
        ),
        migrations.AddField(
            model_name='student',
            name='region',
            field=lib.fields.ProtectedForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='student_region', to='mazintech.Region'),
        ),
        migrations.AddField(
            model_name='studentdocument',
            name='document_type',
            field=models.ManyToManyField(to='college.StudentDocumentUploads', verbose_name=b'Document Type'),
        ),
        migrations.AddField(
            model_name='studentdocument',
            name='modified_by',
            field=lib.fields.ProtectedForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='modified_student_documents', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='studentdocument',
            name='student',
            field=lib.fields.ProtectedForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='student_documents', to='college.Student'),
        ),
    ]
