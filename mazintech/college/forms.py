import os

from django.conf import settings
from django import forms
from django.db.models import Q
from datetime import datetime, timedelta
from django.db.models.functions import Concat
from django.db.models import Value as V
from django.forms.widgets import *
from models import *
from rest_framework import fields, serializers
from django.core.urlresolvers import reverse
from mazintech.models import *
from dal import autocomplete, forward
from django.forms.widgets import RadioSelect

class CourseForm(forms.ModelForm):
    class Meta:
        model = Course
        fields = ['course_name']


class StudentForm(forms.ModelForm):
    class Meta:
        model = Student
        exclude = ['deleted', 'created_by', 'modified_by']

    def __init__(self, *args, **kwargs):
        super(StudentForm, self).__init__(*args, **kwargs)
        
        for f in self.fields:
            self.fields[f].widget.attrs['required'] = 'true'
        self.fields['date_of_birth'].widget.attrs['class'] = 'date_field'


class EnrolmentForm(forms.ModelForm):
    class Meta:
        model = Enrolment
        exclude = ['deleted', 'created_by', 'modified_by']

    def __init__(self, *args, **kwargs):
        super(EnrolmentForm, self).__init__(*args, **kwargs)
        
        for f in self.fields:
            self.fields[f].widget.attrs['required'] = 'true'
        self.fields['application_signed'].widget.attrs['class'] = 'date_field'


class StudentFilterForm(forms.Form):
    student_name = forms.CharField(required = False, label="Student Name")
    student_surname = forms.CharField(required = False, label="Student Surname")
    date_from = forms.DateTimeField(required=False,label='From Date')
    date_to   = forms.DateTimeField(required=False,label='To Date')
    id_number = forms.CharField(required = False, label="ID Number")
    course = forms.CharField(required = False, label="Course")
    enrolment_year = forms.CharField(required = False, label="Enrolment Year")

    def __init__(self, *args, **kwargs):
        super(StudentFilterForm, self).__init__(*args, **kwargs)

    def filter(self, students):
        if self.cleaned_data is not None:
            if self.cleaned_data['student_name']:
                students = students.filter(first_name__icontains=self.cleaned_data['student_name'])

            if self.cleaned_data['student_surname']:
                students = students.filter(surname__icontains=self.cleaned_data['student_surname'])

            if self.cleaned_data['enrolment_year']:
                students = students.filter(student_enrolment__enrolment_year=self.cleaned_data['enrolment_year'])

            if self.cleaned_data['id_number']:
                students = students.filter(Q(id_number=self.cleaned_data['id_number'])| 
                                           Q(pasport_number=self.cleaned_data['id_number']))
            if self.cleaned_data['course']:
                courses = Course.objects.filter(course_name__icontains=self.cleaned_data['course'])
                students = students.filter(student_enrolment__course__course_name__icontains=self.cleaned_data['course'])

            if self.cleaned_data['date_to']:
                students = students.filter(created_at__lte=self.cleaned_data['date_to'])

            if self.cleaned_data['date_from']:
                students = students.filter(created_at__gte=self.cleaned_data['date_from'])

        return students

class StudentDocumentForm(forms.ModelForm):

    class Meta:
        model = StudentDocument
        fields = ['document_type']

    def __init__(self, *args, **kwargs):
        super(StudentDocumentForm, self).__init__(*args, **kwargs)
        self.fields['document_type'].widget = SelectMultiple()
        self.fields['document_type'].widget.attrs['class'] = 'multi_select'
        self.fields['document_type'].queryset = StudentDocumentUploads.objects.all()


class StudentFileForm(forms.ModelForm):

    class Meta:
        model = Document
        fields = ['document']

class EnrolmentFilterForm(forms.Form):
    course = forms.CharField(required = False, label="Course")
    date_type = forms.ChoiceField(required = False, label = 'Date Type',
                                  choices = (('captured','Date Captured'),
                                             ('signed','Date Signed')))
    student_name = forms.CharField(required = False, label="Student Name")
    student_surname = forms.CharField(required = False, label="Student Surname")
    date_from = forms.DateTimeField(required=False,label='From Date')
    date_to   = forms.DateTimeField(required=False,label='To Date')    
    enrolment_year = forms.CharField(required = False, label="Enrolment Year")

    def __init__(self, *args, **kwargs):
        super(EnrolmentFilterForm, self).__init__(*args, **kwargs)

    def filter(self, students):
        if self.cleaned_data is not None:
            if self.cleaned_data['student_name']:
                students = students.filter(student__first_name__icontains=self.cleaned_data['student_name'])

            if self.cleaned_data['student_surname']:
                students = students.filter(student__surname__icontains=self.cleaned_data['student_surname'])

            if self.cleaned_data['enrolment_year']:
                students = students.filter(enrolment_year=self.cleaned_data['enrolment_year'])

            if self.cleaned_data['course']:
                students = students.filter(course__course_name__icontains=self.cleaned_data['course'])
            
            if self.cleaned_data['date_type'] == "captured":
                if self.cleaned_data['date_to']:
                    students = students.filter(created_at__lte=self.cleaned_data['date_to'])

                if self.cleaned_data['date_from']:
                    students = students.filter(created_at__gte=self.cleaned_data['date_from'])
            else:
                if self.cleaned_data['date_to']:
                    students = students.filter(application_signed__lte=self.cleaned_data['date_to'])

                if self.cleaned_data['date_from']:
                    students = students.filter(application_signed__gte=self.cleaned_data['date_from'])

        return students

class CourseFilterForm(forms.Form):
    course = forms.CharField(required = False, label="Course") 

    def __init__(self, *args, **kwargs):
        super(CourseFilterForm, self).__init__(*args, **kwargs)

    def filter(self, courses):
        if self.cleaned_data is not None:
            if self.cleaned_data['course']:
                courses = courses.filter(course_name__icontains=self.cleaned_data['course'])
        return courses