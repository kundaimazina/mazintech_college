from django.db import models
from lib.models import BaseModel
from lib.fields import ProtectedForeignKey
from django.core.validators import MaxValueValidator, MinValueValidator
from simple_history.models import HistoricalRecords
from datetime import datetime
from datetime import timedelta
from django.db.models import Sum
from django.conf import settings

from django.http import JsonResponse
from django.shortcuts import render
from django.views.generic import View
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import authentication, permissions
from django.contrib.auth.models import User

class Course(BaseModel):
    course_name = models.CharField(max_length=255, unique=True, null=False, blank=False)

    def __unicode__(self):
        return self.course_name

    def delete(self, logged_in_user):
        self.deleted = True
        self.modified_by = logged_in_user
        self.changed_at = datetime.now()
        self.save()

    @property
    def current_enrolment(self):
        current_year = datetime.now().year
        enrolment = self.course_enrolment.filter(enrolment_year=current_year).count()
        return enrolment

    @property
    def total_enrolment(self):
        enrolment = self.course_enrolment.all().count()
        return enrolment

class Student(BaseModel):
    first_name = models.CharField(max_length=255, null=True, blank=True, verbose_name='First Name')
    surname = models.CharField(max_length=255, null=True, blank=True, verbose_name='Surname')
    date_of_birth = models.DateTimeField(null=True, blank=True, verbose_name='Date Of Birth')
    id_number = models.CharField(default=None, max_length=50, null=True, blank=True, verbose_name='ID Number')
    passport_number = models.CharField(default=None, max_length=50, null=True, blank=True, verbose_name='Passport Number')
    gender = models.CharField(max_length=50, null=False, blank=False, verbose_name='Gender',
                                choices=(('male','Male'),('female','Female')))
    region = ProtectedForeignKey('mazintech.Region', null = True, blank = True, related_name='student_region')
    district = ProtectedForeignKey('mazintech.Branch', null = True, blank = True, related_name='student_district')
    address = ProtectedForeignKey('mazintech.Address', null = True, blank = True, related_name='student_address')
    contact = ProtectedForeignKey('mazintech.Contact', null = True, blank = True, related_name='student_contact')
    created_by = ProtectedForeignKey('mazintech.MazintechUser', null=False, blank=False, related_name='created_student')
    modified_by = ProtectedForeignKey('mazintech.MazintechUser', null=True, blank=True, related_name='modified_student')
    history = HistoricalRecords()

    def __unicode__(self):
        return '{} {}'.format(self.first_name,self.surname)

    def delete(self, logged_in_user):
        self.deleted = True
        self.modified_by = logged_in_user
        self.changed_at = datetime.now()
        self.save()

    @property
    def full_name(self):
        return '{} {}'.format(self.first_name,self.surname)
    
    @property
    def course(self):
        course = self.student_enrolment.order_by('-enrolment_year').first()
        return course.course.course_name if course else ''
    
    @property
    def course_year(self):
        course = self.student_enrolment.order_by('-enrolment_year').first()
        return course.enrolment_year if course else ''

class Enrolment(BaseModel):
    student = ProtectedForeignKey('college.Student', null = True, blank = True, related_name='student_enrolment')
    course = ProtectedForeignKey('college.Course', null = True, blank = True, related_name='course_enrolment')
    application_signed = models.DateTimeField(null=True, blank=True, verbose_name='Date Signed')
    enrolment_year = models.IntegerField(null=False, blank=False, default=0, verbose_name='enrolment_year',
                                  validators=[MinValueValidator(0)])
    application_fee = models.IntegerField(null=False, blank=False, default=0, verbose_name='Application Fee',
                                  validators=[MinValueValidator(0)])
    created_by = ProtectedForeignKey('mazintech.MazintechUser', null=False, blank=False, related_name='created_enrolment')
    modified_by = ProtectedForeignKey('mazintech.MazintechUser', null=True, blank=True, related_name='modified_enrolment')
    history = HistoricalRecords()

    def delete(self, logged_in_user):
        self.deleted = True
        self.modified_by = logged_in_user
        self.changed_at = datetime.now()
        self.save()

class StudentDocumentUploads(BaseModel):
    description = models.CharField(max_length=255, null=False, blank=False,)

    def __unicode__(self):
                return self.description

class StudentDocument(BaseModel):

    student = ProtectedForeignKey('college.Student', null=False, blank=False, related_name='student_documents')
    document = ProtectedForeignKey('mazintech.Document', null=False, blank=False)
    document_type = models.ManyToManyField(StudentDocumentUploads, verbose_name='Document Type')
    created_by = ProtectedForeignKey('mazintech.MazintechUser', null=False, blank=False, related_name='created_student_documents')
    modified_by = ProtectedForeignKey('mazintech.MazintechUser', null=True, blank=True, related_name='modified_student_documents')


# class courseTotalEnrolmentData(APIView):
#     authentication_classes = []
#     permission_classes = []

#     def get(self, request, format=None):
#         courses = Course.objects.exclude(deleted=True).order_by('course_name')
#         totals = []
#         labels = []
#         for course in courses:
#             total = course.total_enrolment
#             label = course.course_name
#             totals.append(total)
#             labels.append(label)

#         data = {
#             'labels':labels, 
#             'values':totals,
#             }

#         return Response(data)

#         