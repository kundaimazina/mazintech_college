# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2018-11-27 22:45
from __future__ import unicode_literals

from django.db import migrations
from django.contrib.auth.models import Group, Permission
import datetime
def creat_groups(self, *args, **options):
    groups = ['Administrator',
              'Mazintech Employee', 
              'College Administrators', 
              'Management',
              'Regional Manager']
    for grp in groups:
        Group.objects.get_or_create(name=grp)

class Migration(migrations.Migration):

    dependencies = [
        ('mazintech', '0001_initial'),
    ]

    operations = [
    	migrations.RunPython(creat_groups)
    ]
